# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Jeferson Lima| [@jefersonlima](https://gitlab.com/jeferson.lima)|

# Documentação

A documentação do projeto pode ser acessada pelo link:

>  https://cursoseaulas.gitlab.io/introducao-engenharia/ie21cp20201

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)
